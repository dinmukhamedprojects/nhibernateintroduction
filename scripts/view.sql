CREATE VIEW [V_Departments_Count] AS
Select D.ID, D.Name, Count(E.FirstName) as "Count"
from Departments D
left join Employees E
on D.ID = E.DepartmentID
group by D.ID, D.Name