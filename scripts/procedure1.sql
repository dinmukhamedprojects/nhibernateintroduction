CREATE PROCEDURE SP_GetManagerEmployees
	@managerID INT 
AS 
    BEGIN
        SELECT  E.ID, E.FirstName, E.LastName
        FROM    Employees E
        WHERE   E.ManagerID = @managerID
    END


	--exec SP_GetManagerEmployees 5