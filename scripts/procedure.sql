CREATE PROCEDURE SP_InsertDepartment 
	@name VARCHAR(55)
AS  
  BEGIN
  INSERT INTO Departments(ID, Name, DirectorID) VALUES ((SELECT MAX(ID) FROM Departments) + 1, @name, null);
  END


  --EXECUTE SP_InsertDepartment N'Ackerman';  