﻿using HumanResources.Database.Xml.Services;
using NUnit.Framework;

namespace HumanResources.Database.Xml.IntegrationTests.Mapping
{
    [Category("Integration")]
    public class DepartmentMapTests
    {
        private DepartmentService _departmentService;

        [SetUp]
        public void Initialize()
        {
            var session = NHibernateHelper.GetCurrentSession();

            _departmentService = new DepartmentService(session);
        }

        [Test]
        public void GetDepartment_Success()
        {
            var department = _departmentService.Get(1);
        }
    }
}