﻿using HumanResources.Database.Xml.Organization;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;
using NUnit.Framework;

namespace HumanResources.Database.Xml.IntegrationTests.Mapping
{
    [Category("Integration")]
    public class GenerateSchemaTests
    {
        [Test]
        public void Can_generate_schema()
        {
            var cfg = new Configuration();
            cfg.Configure();
            cfg.AddAssembly(typeof(Employee).Assembly);

            new SchemaExport(cfg).Execute(false, true, false);
        }
    }
}