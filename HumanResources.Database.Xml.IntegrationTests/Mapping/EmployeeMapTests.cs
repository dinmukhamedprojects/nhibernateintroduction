﻿using HumanResources.Database.Xml.Organization;
using HumanResources.Database.Xml.Services;
using NUnit.Framework;

namespace HumanResources.Database.Xml.IntegrationTests.Mapping
{
    [Category("Integration")]
    public class EmployeeMapTests
    {
        private EmployeeService _employeeService;

        [SetUp]
        public void Initialize()
        {
            var session = NHibernateHelper.GetCurrentSession();

            _employeeService = new EmployeeService(session);
        }

        [Test]
        public void IsEmployeeMappingIsCorrect()
        {
            var session = NHibernateHelper.GetCurrentSession();

            var employee = new Employee()
            {
                FirstName = "test",
                LastName = "testl"
            };

            using var tx = session.BeginTransaction();

            session.Save(employee);

            //new PersistenceSpecification<Employee>(session)
            //    .VerifyTheMappings(employee);

            tx.Rollback();
        }

        [Test]
        public void GetEmployee_Success()
        {
            var employee = _employeeService.Get(1);
        }
    }
}