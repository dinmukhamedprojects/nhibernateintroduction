﻿using HumanResources.Database.Organization.Accounts;
using Simplify.Repository;
using System.Collections.Generic;

namespace HumanResources.Database.Services.Accounts
{
    public class GroupsService
    {
        private readonly IGenericRepository<IGroup> _repository;

        public GroupsService(IGenericRepository<IGroup> repository)
        {
            _repository = repository;
        }

        public IGroup Create(IGroup group)
        {
            _repository.Add(group);

            return group;
        }

        public IGroup Get(int id)
        {
            var result = _repository.GetSingleByID(id);

            return result;
        }

        public IList<IGroup> Get()
        {
            var result = _repository.GetMultipleByQuery();

            return result;
        }

        public void Update(IGroup group)
        {
            _repository.Update(group);
        }

        public void Delete(IGroup group)
        {
            _repository.Delete(group);
        }
    }
}