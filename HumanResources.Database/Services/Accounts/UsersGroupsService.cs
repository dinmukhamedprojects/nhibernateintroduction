﻿using HumanResources.Database.Organization.Accounts;
using Simplify.Repository;
using System.Collections.Generic;

namespace HumanResources.Database.Services.Accounts
{
    public class UsersGroupsService
    {
        private readonly IGenericRepository<IUsersGroups> _repository;

        public UsersGroupsService(IGenericRepository<IUsersGroups> repository)
        {
            _repository = repository;
        }

        public IUsersGroups Create(IUsersGroups usersGroups)
        {
            _repository.Add(usersGroups);

            return usersGroups;
        }

        public IUsersGroups Get(int id)
        {
            var result = _repository.GetSingleByID(id);

            return result;
        }

        public IList<IUsersGroups> Get()
        {
            var result = _repository.GetMultipleByQuery();

            return result;
        }

        public void Update(IUsersGroups usersGroups)
        {
            _repository.Update(usersGroups);
        }

        public void Delete(IUsersGroups usersGroups)
        {
            _repository.Delete(usersGroups);
        }
    }
}