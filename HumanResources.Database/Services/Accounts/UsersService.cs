﻿using HumanResources.Database.Organization.Accounts;
using Simplify.Repository;
using System.Collections.Generic;
using System.Linq;

namespace HumanResources.Database.Services.Accounts
{
    public class UsersService
    {
        private readonly IGenericRepository<IUser> _repository;

        public UsersService(IGenericRepository<IUser> repository)
        {
            _repository = repository;
        }

        public IUser Create(IUser user)
        {
            _repository.Add(user);

            return user;
        }

        public IUser Get(int id)
        {
            var result = _repository.GetSingleByID(id);

            return result;
        }

        public IList<IUser> Get()
        {
            var result = _repository.GetMultipleByQuery();

            return result;
        }

        public void GetGroupByName()
        {
            var result = _repository.GetMultipleByQuery().GroupBy(x => x.Name);
        }

        public void Update(IUser user)
        {
            _repository.Update(user);
        }

        public void Delete(IUser user)
        {
            _repository.Delete(user);
        }
    }
}