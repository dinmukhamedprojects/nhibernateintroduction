﻿using HumanResources.Database.Organization;
using Simplify.Repository;
using System.Threading.Tasks;

namespace HumanResources.Database.Services
{
    public class EmployeeTranscRepository
    {
        private readonly IGenericRepository<IEmployee> _repository;

        public EmployeeTranscRepository(IGenericRepository<IEmployee> repository)
        {
            _repository = repository;
        }

        public IEmployee Create(IEmployee user)
        {
            _repository.Add(user);

            return user;
        }

        public IEmployee Get(int id)
        {
            var result = _repository.GetFirstByQuery(x => x.ID == id);

            return result;
        }

        public void Update(IEmployee user)
        {
            _repository.Update(user);
        }

        public void Delete(IEmployee user)
        {
            _repository.Delete(user);
        }

        public async Task<IEmployee> GetAsync(int id)
        {
            var items = await _repository.GetSingleByIDAsync(id);

            return items;
        }
    }
}