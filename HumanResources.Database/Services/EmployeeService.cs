﻿using HumanResources.Database.Organization;
using System.Collections.Generic;
using System.Linq;

namespace HumanResources.Database.Services
{
    public class EmployeeService
    {
        private readonly HumanResourcesUnitOfWork _unitOfWork;

        public EmployeeService(HumanResourcesUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IEmployee Create(IEmployee user)
        {
            _unitOfWork.BeginTransaction();

            _unitOfWork.Session.Save(user);

            _unitOfWork.Commit();

            return user;
        }

        public IEmployee Get(int id)
        {
            _unitOfWork.BeginTransaction();

            var result = _unitOfWork.Session.Query<Employee>().SingleOrDefault(x => x.ID == id);

            _unitOfWork.Commit();

            return result;
        }

        public void GetGroupBy()
        {
            _unitOfWork.BeginTransaction();

            var result = _unitOfWork.Session.Query<Employee>()
                .Where(x => x.Manager != null)
                .GroupBy(x => new
                {
                    x.Department.ID,
                    x.Department.Name
                })
                .Select(s => new
                {
                    Dept = s.Key.ID,
                    DeptName = s.Key.Name,
                    EmployeeCount = s.Count()
                }).Take(1);

            _unitOfWork.Commit();
        }

        public void Update(IEmployee user)
        {
            _unitOfWork.BeginTransaction();

            _unitOfWork.Session.SaveOrUpdate(user);

            _unitOfWork.Commit();
        }

        public void Delete(IEmployee user)
        {
            _unitOfWork.BeginTransaction();

            _unitOfWork.Session.Delete(user);

            _unitOfWork.Commit();
        }

        public IList<IEmployee> GetManagersOfDepartment(IDepartment department)
        {
            _unitOfWork.BeginTransaction();

            var result = _unitOfWork.Session.Query<Employee>().Where(x => x.Department == department && x.Manager == null).Cast<IEmployee>().ToList();

            _unitOfWork.Commit();

            return result;
        }

        public IList<IEmployee> GetManagers()
        {
            _unitOfWork.BeginTransaction();

            var result = _unitOfWork.Session.Query<Employee>().Where(x => x.Manager == null).Cast<IEmployee>().ToList();

            _unitOfWork.Commit();

            return result;
        }
    }
}