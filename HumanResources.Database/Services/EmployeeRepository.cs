﻿using HumanResources.Database.Organization;
using Simplify.Repository;

namespace HumanResources.Database.Services
{
    public class EmployeeRepository
    {
        private readonly IHumanResourcesUnitOfWork _unitOfWork;
        private readonly IGenericRepository<IEmployee> _repository;

        public EmployeeRepository(IHumanResourcesUnitOfWork unitOfWork, IGenericRepository<IEmployee> repository)
        {
            _unitOfWork = unitOfWork;
            _repository = repository;
        }

        public IEmployee Create(IEmployee user)
        {
            _unitOfWork.BeginTransaction();

            _repository.Add(user);

            _unitOfWork.Commit();

            return user;
        }

        public IEmployee Get(int id)
        {
            _unitOfWork.BeginTransaction();

            var result = _repository.GetFirstByQuery(x => x.ID == id);

            _unitOfWork.Commit();

            return result;
        }

        public void Update(IEmployee user)
        {
            _unitOfWork.BeginTransaction();

            _repository.Update(user);

            _unitOfWork.Commit();
        }

        public void Delete(IEmployee user)
        {
            _unitOfWork.BeginTransaction();

            _repository.Delete(user);

            _unitOfWork.Commit();
        }
    }
}