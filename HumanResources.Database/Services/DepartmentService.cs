﻿using HumanResources.Database.Organization;
using System.Linq;

namespace HumanResources.Database.Services
{
    public class DepartmentService
    {
        private readonly HumanResourcesUnitOfWork _unitOfWork;

        public DepartmentService(HumanResourcesUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IDepartment Create(string name)
        {
            _unitOfWork.BeginTransaction();

            var department = new Department { Name = name };

            _unitOfWork.Session.Save(department);

            _unitOfWork.Commit();

            return department;
        }

        public IDepartment Get(string name)
        {
            _unitOfWork.BeginTransaction();

            var result = _unitOfWork.Session.Query<Department>().SingleOrDefault(x => x.Name == name);

            _unitOfWork.Commit();

            return result;
        }

        public void Update(IDepartment department)
        {
            _unitOfWork.BeginTransaction();

            _unitOfWork.Session.SaveOrUpdate(department);

            _unitOfWork.Commit();
        }

        public void Delete(IDepartment department)
        {
            if (department == null) return;

            _unitOfWork.BeginTransaction();

            _unitOfWork.Session.Delete(department);

            _unitOfWork.Commit();
        }
    }
}