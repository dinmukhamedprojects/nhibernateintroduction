﻿using NHibernate;
using System;
using System.Data;
using System.Threading.Tasks;

namespace HumanResources.Database
{
    public class HumanResourcesUnitOfWork : IHumanResourcesUnitOfWork
    {
        private ITransaction _transaction;

        public HumanResourcesUnitOfWork(ISessionFactory sessionFactory)
        {
            Session = sessionFactory.OpenSession();
            Session.FlushMode = FlushMode.Commit;
        }

        public ISession Session { get; }

        public void BeginTransaction(IsolationLevel isolationLevel = IsolationLevel.ReadCommitted)
        {
            _transaction = Session.BeginTransaction(isolationLevel);
        }

        public void Commit()
        {
            if (!_transaction.IsActive)
                throw new InvalidOperationException("Oops! We don't have an active transaction");

            _transaction.Commit();
        }

        public Task CommitAsync()
        {
            if (!_transaction.IsActive)
                throw new InvalidOperationException("Oops! We don't have an active transaction");

            return _transaction.CommitAsync();
        }

        public void Rollback()
        {
            if (_transaction.IsActive)
                _transaction.Rollback();
        }

        public Task RollbackAsync()
        {
            if (!_transaction.IsActive)
                throw new InvalidOperationException("Oops! We don't have an active transaction");

            return _transaction.RollbackAsync();
        }

        public void Dispose()
        {
            if (Session.IsOpen)
                Session.Close();
        }
    }
}