﻿namespace HumanResources.Database.Organization
{
    public class Employee : IEmployee
    {
        public virtual int ID { get; set; }
        public virtual string FirstName { get; set; }
        public virtual string LastName { get; set; }
        public virtual IDepartment Department { get; set; }
        public virtual IEmployee Manager { get; set; }
    }
}