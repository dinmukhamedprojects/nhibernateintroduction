﻿using System.Collections.Generic;

namespace HumanResources.Database.Organization
{
    public interface IDepartment
    {
        int ID { get; set; }
        string Name { get; set; }

        IList<IEmployee> Employees { get; set; }

        IEmployee Director { get; set; }
    }
}