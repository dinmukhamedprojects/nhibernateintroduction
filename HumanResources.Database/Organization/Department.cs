﻿using System.Collections.Generic;

namespace HumanResources.Database.Organization
{
    public class Department : IDepartment
    {
        public virtual int ID { get; set; }
        public virtual string Name { get; set; }
        public virtual IList<IEmployee> Employees { get; set; } = new List<IEmployee>();
        public virtual IEmployee Director { get; set; }
    }
}