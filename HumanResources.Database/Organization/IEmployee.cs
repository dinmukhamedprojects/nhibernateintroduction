﻿namespace HumanResources.Database.Organization
{
    public interface IEmployee
    {
        int ID { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        IDepartment Department { get; set; }
        IEmployee Manager { get; set; }
    }
}