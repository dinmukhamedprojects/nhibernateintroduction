﻿namespace HumanResources.Database.Organization
{
    public class DepartmentCountView
    {
        public virtual int ID { get; set; }
        public virtual string Name { get; set; }
        public virtual int EmployeesCount { get; set; }
    }
}