﻿using System.Collections.Generic;

namespace HumanResources.Database.Organization.Accounts
{
    public class Group : IGroup
    {
        public virtual int ID { get; set; }

        public virtual string Name { get; set; }

        public virtual IList<Privilege> Privileges { get; set; } = new List<Privilege>();

        public virtual IList<IUsersGroups> Users { get; set; } = new List<IUsersGroups>();
    }
}