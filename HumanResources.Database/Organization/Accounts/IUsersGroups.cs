﻿namespace HumanResources.Database.Organization.Accounts
{
    public interface IUsersGroups
    {
        int ID { get; set; }
        IUser User { get; set; }
        IGroup Group { get; set; }
    }
}