﻿namespace HumanResources.Database.Organization.Accounts
{
    public class UsersGroups : IUsersGroups
    {
        public virtual int ID { get; set; }

        public virtual IUser User { get; set; }

        public virtual IGroup Group { get; set; }
    }
}