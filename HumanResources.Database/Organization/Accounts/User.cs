﻿using System;
using System.Collections.Generic;

namespace HumanResources.Database.Organization.Accounts
{
    public class User : IUser
    {
        public virtual int ID { get; set; }

        public virtual string Name { get; set; }

        public virtual string Password { get; set; }

        public virtual string EMail { get; set; }

        public virtual DateTime LastActivityTime { get; set; }

        public virtual IList<Privilege> Privileges { get; set; } = new List<Privilege>();

        public virtual IList<IUsersGroups> Groups { get; set; } = new List<IUsersGroups>();
    }
}