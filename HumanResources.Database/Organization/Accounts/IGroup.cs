﻿using System.Collections.Generic;

namespace HumanResources.Database.Organization.Accounts
{
    public interface IGroup
    {
        int ID { get; set; }
        string Name { get; set; }
        IList<Privilege> Privileges { get; set; }
        IList<IUsersGroups> Users { get; set; }
    }
}