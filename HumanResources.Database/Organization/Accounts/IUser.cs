﻿using System;
using System.Collections.Generic;

namespace HumanResources.Database.Organization.Accounts
{
    public interface IUser
    {
        int ID { get; set; }
        string Name { get; set; }
        string Password { get; set; }
        string EMail { get; set; }
        DateTime LastActivityTime { get; set; }
        IList<Privilege> Privileges { get; set; }
        IList<IUsersGroups> Groups { get; set; }
    }
}