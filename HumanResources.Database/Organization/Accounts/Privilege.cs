﻿namespace HumanResources.Database.Organization.Accounts
{
    public enum Privilege
    {
        UserManagement = 0,
        GroupManagement = 1,
        OrganizationManagement = 2
    }
}