﻿using Microsoft.Extensions.Configuration;
using System;

namespace HumanResources.Database
{
    public static class ConfigurationBuilder
    {
        /// <summary>
        /// Builds the Air Astana global configuration
        /// </summary>
        /// <param name="additionalConfiguration">The additional configuration.</param>
        /// <returns></returns>
        public static IConfiguration Build(Func<IConfigurationBuilder, IConfigurationBuilder> additionalConfiguration = null)
        {
            var builder = new Microsoft.Extensions.Configuration.ConfigurationBuilder();

            builder.AddJsonFile("appsettings.json", true, true);

            additionalConfiguration?.Invoke(builder);

            return builder.Build();
        }
    }
}