﻿using FluentNHibernate.Cfg;
using FluentNHibernate.Conventions.Helpers;
using Microsoft.Extensions.Configuration;
using NHibernate;
using Simplify.FluentNHibernate;
using Simplify.FluentNHibernate.Conventions;
using System.Data;

namespace HumanResources.Database
{
    public class HumanResourcesSessionFactoryBuilder
    {
        private readonly IConfiguration _configuration;
        private readonly string _configSectionName;
        private readonly FluentConfiguration _fluentConfiguration = Fluently.Configure();

        /// <summary>
        /// Initializes a new instance of the <see cref="HumanResourcesSessionFactoryBuilder"/> class.
        /// </summary>
        /// <param name="configuration">The configuration.</param>
        /// <param name="configSectionName">Name of the configuration section.</param>
        public HumanResourcesSessionFactoryBuilder(IConfiguration configuration, string configSectionName = "HumanResourcesDatabaseConnectionSettings")
        {
            _configuration = configuration;
            _configSectionName = configSectionName;
        }

        /// <summary>
        /// Gets the session factory.
        /// </summary>
        /// <value>
        /// The session factory.
        /// </value>
        public ISessionFactory Instance { get; private set; }

        /// <summary>
        /// Builds the session factory.
        /// </summary>
        public HumanResourcesSessionFactoryBuilder Build()
        {
            var configuration = CreateConfiguration();

            Instance = configuration.BuildSessionFactory();

            return this;
        }

        /// <summary>
        /// Creates the configuration.
        /// </summary>
        /// <returns></returns>
        public FluentConfiguration CreateConfiguration()
        {
            var configuration = _fluentConfiguration
                .InitializeFromConfigMsSql(_configuration, _configSectionName, c => c.IsolationLevel(IsolationLevel.ReadUncommitted))
                .AddMappingsFromAssemblyOf<HumanResourcesSessionFactoryBuilder>(
                    PrimaryKey.Name.Is(x => "ID"),
                    Table.Is(x => x.EntityType.Name + "s"),
                    ForeignKey.EndsWith("ID"),
                    ForeignKeyConstraintNameConvention.WithConstraintNameConvention(),
                    DefaultCascade.None());

            return configuration;
        }
    }
}