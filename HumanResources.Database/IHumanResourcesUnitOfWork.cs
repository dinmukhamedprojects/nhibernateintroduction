﻿using Simplify.Repository;

namespace HumanResources.Database
{
    public interface IHumanResourcesUnitOfWork : ITransactUnitOfWork
    {
    }
}