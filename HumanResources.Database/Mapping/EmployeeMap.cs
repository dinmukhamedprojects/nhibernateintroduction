﻿using FluentNHibernate.Mapping;
using HumanResources.Database.Organization;

namespace HumanResources.Database.Mapping
{
    public class EmployeeMap : ClassMap<Employee>
    {
        public EmployeeMap()
        {
            Table("Employees");

            Id(x => x.ID).GeneratedBy.Increment();

            Map(x => x.FirstName);
            Map(x => x.LastName);

            References<Department>(x => x.Department).Not.Nullable();
            References<Employee>(x => x.Manager).Column("ManagerID");
        }
    }
}