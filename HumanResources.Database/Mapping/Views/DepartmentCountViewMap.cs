﻿using FluentNHibernate.Mapping;
using HumanResources.Database.Organization;

namespace HumanResources.Database.Mapping.Views
{
    public class DepartmentCountViewMap : ClassMap<DepartmentCountView>
    {
        public DepartmentCountViewMap()
        {
            Table("V_Departments_Count");

            ReadOnly();

            Id(x => x.ID).GeneratedBy.Assigned();

            Map(x => x.Name);
            Map(x => x.EmployeesCount).Column("Count");
        }
    }
}