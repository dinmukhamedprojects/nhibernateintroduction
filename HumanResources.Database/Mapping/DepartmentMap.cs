﻿using FluentNHibernate.Mapping;
using HumanResources.Database.Organization;

namespace HumanResources.Database.Mapping
{
    public class DepartmentMap : ClassMap<Department>
    {
        public DepartmentMap()
        {
            Table("Departments");

            Id(x => x.ID).GeneratedBy.Increment();

            Map(x => x.Name).Unique();

            HasMany<Employee>(x => x.Employees).Cascade.AllDeleteOrphan();
            References<Employee>(x => x.Director).Column("DirectorID");
        }
    }
}