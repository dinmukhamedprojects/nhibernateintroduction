﻿using FluentNHibernate.Mapping;
using HumanResources.Database.Organization.Accounts;

namespace HumanResources.Database.Mapping.Accounts
{
    public class GroupMap : ClassMap<Group>
    {
        public GroupMap()
        {
            Table("Groups");

            Id(x => x.ID);

            Map(x => x.Name);

            HasMany<UsersGroups>(x => x.Users).Cascade.All();

            HasMany(x => x.Privileges)
                .KeyColumn("GroupID")
                .ForeignKeyConstraintName("FK_Custom_UsersPrivileges_GroupID")
                .Table("GroupsPrivileges")
                .Element("Type");
        }
    }
}