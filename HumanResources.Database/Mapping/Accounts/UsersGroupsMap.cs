﻿using FluentNHibernate.Mapping;
using HumanResources.Database.Organization.Accounts;

namespace HumanResources.Database.Mapping.Accounts
{
    public class UsersGroupsMap : ClassMap<UsersGroups>
    {
        public UsersGroupsMap()
        {
            Table("UsersGroups");

            Id(x => x.ID);
            References<User>(x => x.User).Cascade.All();
            References<Group>(x => x.Group).Cascade.All();
        }
    }
}