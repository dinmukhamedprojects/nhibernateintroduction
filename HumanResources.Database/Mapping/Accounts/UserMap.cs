﻿using FluentNHibernate.Mapping;
using HumanResources.Database.Organization.Accounts;

namespace HumanResources.Database.Mapping.Accounts
{
    public class UserMap : ClassMap<User>
    {
        public UserMap()
        {
            Table("Users");

            Id(x => x.ID);

            Map(x => x.Name);

            Map(x => x.Password);
            Map(x => x.EMail);
            Map(x => x.LastActivityTime);

            HasMany<UsersGroups>(x => x.Groups).Cascade.All().Inverse();

            HasMany(x => x.Privileges)
                .KeyColumn("UserID")
                .ForeignKeyConstraintName("FK_UsersPrivileges_UserID")
                .Table("UsersPrivileges")
                .Element("Type");
        }
    }
}