﻿using NHibernate.Transform;
using System.Collections.Generic;
using System.Linq;

namespace HumanResources.Database.StoredProcedures
{
    public class EmployeeProcedure
    {
        private readonly HumanResourcesUnitOfWork _unitOfWork;

        public EmployeeProcedure(HumanResourcesUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IList<Employee> Execute(int managerID)
        {
            _unitOfWork.BeginTransaction();

            var result = _unitOfWork.Session.CreateSQLQuery($"exec SP_GetManagerEmployees {managerID}")
                .SetResultTransformer(Transformers.AliasToBean<Employee>())
                .List<Employee>()
                .ToList();

            _unitOfWork.Commit();

            return result;
        }
    }
}