﻿namespace HumanResources.Database.StoredProcedures
{
    public class DepartmentProcedure
    {
        private readonly HumanResourcesUnitOfWork _unitOfWork;

        public DepartmentProcedure(HumanResourcesUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public void Execute(string name)
        {
            _unitOfWork.BeginTransaction();

            var query = _unitOfWork.Session.CreateSQLQuery("exec SP_InsertDepartment  @name=:name");
            query.SetString("name", name);

            query.ExecuteUpdate();

            _unitOfWork.Commit();
        }
    }
}