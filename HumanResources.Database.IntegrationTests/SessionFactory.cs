﻿using NHibernate;
using NUnit.Framework;

namespace HumanResources.Database.IntegrationTests
{
    public class SessionFactory
    {
        private static HumanResourcesSessionFactoryBuilder _factoryManager;

        public static void Initialize()
        {
            var config = ConfigurationBuilder.Build();
            _factoryManager = new HumanResourcesSessionFactoryBuilder(config);
            _factoryManager.Build();
        }

        public static ISession GetSession()
        {
            return _factoryManager.Instance.OpenSession();
        }

        public static ISessionFactory GetSessionFactory()
        {
            return _factoryManager.Instance;
        }

        public static HumanResourcesUnitOfWork GetUnitOfWork()
        {
            return new HumanResourcesUnitOfWork(GetSessionFactory());
        }
    }

    [SetUpFixture]
    public class SessionFactorySetup
    {
        [OneTimeSetUp]
        public void Initialize()
        {
            SessionFactory.Initialize();
        }
    }
}