﻿using FluentNHibernate.Testing;
using HumanResources.Database.Organization;
using NUnit.Framework;

namespace HumanResources.Database.IntegrationTests.Mapping
{
    [TestFixture]
    [Category("Integration")]
    public class EmployeeMapTests
    {
        [Test]
        public void IsEmployeeMappingIsCorrect()
        {
            var session = SessionFactory.GetSession();

            var employee = new Employee()
            {
                FirstName = "test",
                LastName = "testl"
            };

            using var tx = session.BeginTransaction();

            session.Save(employee);

            new PersistenceSpecification<IEmployee>(session)
                .VerifyTheMappings(employee);

            tx.Rollback();
        }
    }
}