﻿using NHibernate.Cfg;
using NUnit.Framework;

namespace HumanResources.Database.IntegrationTests.Dangerous
{
    [TestFixture]
    [Category("Integration")]
    public class SchemaUpdate
    {
        [Test]
        public void UpdateSchema()
        {
            var cfg = ConfigurationBuilder.Build();
            var configuration = new HumanResourcesSessionFactoryBuilder(cfg).CreateConfiguration();

            Configuration config = null;
            configuration.ExposeConfiguration(c =>
            {
                config = c;
            });

            configuration.BuildSessionFactory();

            new NHibernate.Tool.hbm2ddl.SchemaUpdate(config).Execute(true, true);
        }
    }
}