﻿using HumanResources.Database.Organization.Accounts;
using HumanResources.Database.Services.Accounts;
using NUnit.Framework;
using Simplify.Repository;
using Simplify.Repository.FluentNHibernate;
using System.Linq;

namespace HumanResources.Database.IntegrationTests.Services.Accounts
{
    [TestFixture]
    [Category("Integration")]
    public class GroupsServiceTests
    {
        private GroupsService _groupsService;
        private UsersService _usersService;
        private UsersGroupsService _usersGroupsService;

        [SetUp]
        public void Initialize()
        {
            var unitOfWork = SessionFactory.GetUnitOfWork();

            _groupsService = new GroupsService(
                new TransactRepository<IGroup>(
                    new GenericRepository<IGroup>(unitOfWork.Session),
                    unitOfWork));

            _usersService = new UsersService(
                new TransactRepository<IUser>(
                    new GenericRepository<IUser>(unitOfWork.Session),
                    unitOfWork));

            _usersGroupsService = new UsersGroupsService(
                new TransactRepository<IUsersGroups>(
                    new GenericRepository<IUsersGroups>(unitOfWork.Session),
                    unitOfWork));
        }

        [Test]
        public void CreateGroup_Success()
        {
            var group = new Group { Name = "Main" };

            _groupsService.Create(group);
        }

        [Test]
        public void UpdateGroup_AddUser_Success()
        {
            var group = _groupsService.Get(1);
            var user = _usersService.Get().FirstOrDefault();

            var usersGroups = new UsersGroups
            {
                User = user,
                Group = group
            };

            group.Users.Add(usersGroups);

            _groupsService.Update(group);
        }

        [Test]
        public void UpdateGroup_RemoveUser_Success()
        {
            var group = _groupsService.Get(1);

            group.Users.RemoveAt(group.Users.IndexOf(group.Users.LastOrDefault()));

            _groupsService.Update(group);
        }
    }
}