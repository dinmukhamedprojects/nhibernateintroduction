﻿using HumanResources.Database.Organization.Accounts;
using HumanResources.Database.Services.Accounts;
using NUnit.Framework;
using Simplify.Repository;
using Simplify.Repository.FluentNHibernate;
using System.Collections.Generic;
using System.Linq;

namespace HumanResources.Database.IntegrationTests.Services.Accounts
{
    [TestFixture]
    [Category("Integration")]
    public class UsersServiceTests
    {
        private UsersService _usersService;
        private GroupsService _groupsService;
        private UsersGroupsService _service;

        [SetUp]
        public void Initialize()
        {
            var unitOfWork = SessionFactory.GetUnitOfWork();

            _usersService = new UsersService(
                new TransactRepository<IUser>(
                    new GenericRepository<IUser>(unitOfWork.Session),
                    unitOfWork));

            _groupsService = new GroupsService(
                new TransactRepository<IGroup>(
                    new GenericRepository<IGroup>(unitOfWork.Session),
                    unitOfWork));

            _service = new UsersGroupsService(
                new TransactRepository<IUsersGroups>(
                    new GenericRepository<IUsersGroups>(unitOfWork.Session),
                    unitOfWork));
        }

        [Test]
        public void CreateUser_Success()
        {
            var group = new Group { Name = "Base" };

            var user = new User
            {
                Name = "Test",
                EMail = "",
                //Groups = new List<IGroup>() { group },
                Password = "asd",
                Privileges = new List<Privilege> { Privilege.GroupManagement, Privilege.GroupManagement }
            };

            _usersService.Create(user);
        }

        [Test]
        public void UpdateGroup_AddUser_Success()
        {
            var user = _usersService.Get().FirstOrDefault();

            var group = _groupsService.Get().FirstOrDefault();

            //user.Groups.Add(group);

            _usersService.Update(user);
        }

        [Test]
        public void UpdateUserRemovePrivileges_Success()
        {
            var user = _usersService.Get(2);

            user.Privileges = new List<Privilege>();

            _usersService.Update(user);
        }

        [Test]
        public void UsersGroupsByName()
        {
            _usersService.GetGroupByName();
        }
    }
}