﻿using HumanResources.Database.Organization;
using HumanResources.Database.Services;
using HumanResources.Database.StoredProcedures;
using NUnit.Framework;
using System.Linq;

namespace HumanResources.Database.IntegrationTests.Services
{
    [TestFixture]
    [Category("Integration")]
    public class WorkWithDbTests
    {
        private DepartmentProcedure _departmentProcude;
        private EmployeeProcedure _employeeProcedure;
        private EmployeeService _employeeService;

        [SetUp]
        public void Initialize()
        {
            var unitOfWork = SessionFactory.GetUnitOfWork();

            _departmentProcude = new DepartmentProcedure(unitOfWork);
            _employeeProcedure = new EmployeeProcedure(unitOfWork);
            _employeeService = new EmployeeService(unitOfWork);
        }

        [Test]
        public void SelectView()
        {
            var session = SessionFactory.GetSession();

            using var tx = session.BeginTransaction();

            var result = session.Query<DepartmentCountView>().ToList();

            tx.Commit();
        }

        [Test]
        public void ExecuteDepartmentStoredProcedure()
        {
            _departmentProcude.Execute("testik");
        }

        [Test]
        public void ExecuteEmployeeStoredProcedure()
        {
            var managers = _employeeService.GetManagers();

            var employees = _employeeProcedure.Execute(managers.FirstOrDefault().ID);
        }

        [Test]
        public void SelectExmaples_Success()
        {
            var session = SessionFactory.GetSession();

            using var tx = session.BeginTransaction();

            var result = session.Query<DepartmentCountView>().ToList();

            tx.Commit();
        }
    }
}