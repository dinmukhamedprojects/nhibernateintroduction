﻿using HumanResources.Database.Organization;
using HumanResources.Database.Services;
using NUnit.Framework;
using Simplify.Repository;
using Simplify.Repository.FluentNHibernate;
using System.Threading.Tasks;

namespace HumanResources.Database.IntegrationTests.Services
{
    [TestFixture]
    [Category("Integration")]
    public class RepositoryTests
    {
        private EmployeeRepository _employeeService;
        private EmployeeTranscRepository _employeeTranscService;

        [SetUp]
        public void Initialize()
        {
            var unitOfWork = SessionFactory.GetUnitOfWork();

            _employeeService = new EmployeeRepository(
                unitOfWork,
                new GenericRepository<IEmployee>(unitOfWork.Session));

            _employeeTranscService = new EmployeeTranscRepository(
                new TransactRepository<IEmployee>(
                    new GenericRepository<IEmployee>(unitOfWork.Session),
                    unitOfWork));
        }

        [Test]
        public void GetEmplopyee_Success()
        {
            var employee = _employeeService.Get(1);
        }

        [Test]
        public void GeetEmplopyee_Transc_Success()
        {
            var employee = _employeeTranscService.Get(1);
        }

        [Test]
        public async Task GetEmplopyeeAsync_Success()
        {
            var employee = await _employeeTranscService.GetAsync(1);
        }
    }
}