﻿using HumanResources.Database.Services;
using HumanResources.Database.StoredProcedures;
using NUnit.Framework;
using Employee = HumanResources.Database.Organization.Employee;

namespace HumanResources.Database.IntegrationTests.Services
{
    [TestFixture]
    [Category("Integration")]
    public class DepartmentServiceTests
    {
        private const string departmentNameIT = "Information Technologies Department";
        private const string departmentNameHR = "Human Resources Department";
        private const string departmentNameSales = "Sales Department";
        private DepartmentService _departmentService;
        private EmployeeService _employeeService;
        private DepartmentProcedure _departmentProcude;
        private EmployeeProcedure _employeeProcedure;

        [SetUp]
        public void Initialize()
        {
            var unitOfWork = SessionFactory.GetUnitOfWork();

            _departmentService = new DepartmentService(unitOfWork);
            _employeeService = new EmployeeService(unitOfWork);
            _departmentProcude = new DepartmentProcedure(unitOfWork);
            _employeeProcedure = new EmployeeProcedure(unitOfWork);
        }

        [Test]
        public void InsertingDepartments()
        {
            _departmentService.Create(departmentNameIT);
            _departmentService.Create(departmentNameHR);
            _departmentService.Create(departmentNameSales);
        }

        [Test]
        public void DeletingDepartments()
        {
            _departmentService.Delete(_departmentService.Get(departmentNameIT));
            _departmentService.Delete(_departmentService.Get(departmentNameHR));
            _departmentService.Delete(_departmentService.Get(departmentNameSales));
        }

        [Test]
        public void SelectingDepartments()
        {
            _departmentService.Get(departmentNameIT);
            _departmentService.Get(departmentNameHR);
            _departmentService.Get(departmentNameSales);
        }

        [Test]
        public void InsertingEmployeesIT()
        {
            var departmentIT = _departmentService.Get(departmentNameIT);

            var employeedirector = new Employee() { FirstName = "test4", LastName = "test4", Department = departmentIT };
            departmentIT.Director = employeedirector;
            var employeemanager = new Employee() { FirstName = "test5", LastName = "test5", Department = departmentIT, Manager = employeedirector };
            var employee = new Employee() { FirstName = "test", LastName = "test", Department = departmentIT, Manager = employeemanager };
            var employee1 = new Employee() { FirstName = "test1", LastName = "test1", Department = departmentIT, Manager = employeemanager };
            var employee2 = new Employee() { FirstName = "test2", LastName = "test2", Department = departmentIT, Manager = employeemanager };
            var employee3 = new Employee() { FirstName = "test3", LastName = "test3", Department = departmentIT, Manager = employeemanager };

            departmentIT.Employees.Add(employee);
            departmentIT.Employees.Add(employee1);
            departmentIT.Employees.Add(employee2);
            departmentIT.Employees.Add(employee3);
            departmentIT.Employees.Add(employeemanager);
            departmentIT.Employees.Add(employeedirector);

            _departmentService.Update(departmentIT);
        }

        [Test]
        public void InsertingEmployeesHR()
        {
            #region HR Department

            var departmentIT = _departmentService.Get(departmentNameHR);

            var employeedirector = new Employee() { FirstName = "test4", LastName = "test4", Department = departmentIT };
            departmentIT.Director = employeedirector;
            var employeemanager = new Employee() { FirstName = "test5", LastName = "test5", Department = departmentIT, Manager = employeedirector };
            var employee = new Employee() { FirstName = "test", LastName = "test", Department = departmentIT, Manager = employeemanager };
            var employee1 = new Employee() { FirstName = "test1", LastName = "test1", Department = departmentIT, Manager = employeemanager };

            departmentIT.Employees.Add(employee);
            departmentIT.Employees.Add(employee1);
            departmentIT.Employees.Add(employeemanager);
            departmentIT.Employees.Add(employeedirector);

            _departmentService.Update(departmentIT);

            #endregion HR Department
        }

        [Test]
        public void InsertingEmployeesSales()
        {
            #region Sales Department

            var departmentIT = _departmentService.Get(departmentNameSales);

            var employeedirector = new Employee() { FirstName = "test4", LastName = "test4", Department = departmentIT };
            departmentIT.Director = employeedirector;
            var employeemanager = new Employee() { FirstName = "test5", LastName = "test5", Department = departmentIT, Manager = employeedirector };
            var employee = new Employee() { FirstName = "test", LastName = "test", Department = departmentIT, Manager = employeemanager };
            var employee1 = new Employee() { FirstName = "test1", LastName = "test1", Department = departmentIT, Manager = employeemanager };
            var employee2 = new Employee() { FirstName = "test2", LastName = "test2", Department = departmentIT, Manager = employeemanager };
            var employee3 = new Employee() { FirstName = "test3", LastName = "test3", Department = departmentIT, Manager = employeemanager };
            var employee4 = new Employee() { FirstName = "test7", LastName = "test7", Department = departmentIT, Manager = employeemanager };
            var employee5 = new Employee() { FirstName = "test8", LastName = "test8", Department = departmentIT, Manager = employeemanager };

            departmentIT.Employees.Add(employee);
            departmentIT.Employees.Add(employee1);
            departmentIT.Employees.Add(employee2);
            departmentIT.Employees.Add(employee3);
            departmentIT.Employees.Add(employee4);
            departmentIT.Employees.Add(employee5);
            departmentIT.Employees.Add(employeemanager);
            departmentIT.Employees.Add(employeedirector);

            _departmentService.Update(departmentIT);

            #endregion Sales Department
        }
    }
}