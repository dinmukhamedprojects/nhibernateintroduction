﻿using HumanResources.Database.Organization;
using HumanResources.Database.Services;
using NUnit.Framework;
using System.Linq;

namespace HumanResources.Database.IntegrationTests.Services
{
    [TestFixture]
    [Category("Integration")]
    public class EmployeeServiceTests
    {
        private const string departmentNameIT = "Information Technologies Department";
        private DepartmentService _departmentService;
        private EmployeeService _employeeService;

        [SetUp]
        public void Initialize()
        {
            var unitOfWork = SessionFactory.GetUnitOfWork();

            _departmentService = new DepartmentService(unitOfWork);
            _employeeService = new EmployeeService(unitOfWork);
        }

        [Test]
        public void InsertingEmployee()
        {
            var department = _departmentService.Get(departmentNameIT);

            var managers = _employeeService.GetManagersOfDepartment(department);

            var employee = new Employee
            {
                FirstName = "test",
                LastName = "test",
                Department = department,
                Manager = managers.FirstOrDefault()
            };

            _employeeService.Create(employee);
        }

        [Test]
        public void UpdateEmployee()
        {
            var employee = _employeeService.Get(5);

            employee.Manager = null;

            _employeeService.Update(employee);
        }

        [Test]
        public void DeleteEmployee()
        {
            var employee = _employeeService.Get(5);

            _employeeService.Delete(employee);
        }

        [Test]
        public void EmployeesGroupby()
        {
            _employeeService.GetGroupBy();
        }
    }
}