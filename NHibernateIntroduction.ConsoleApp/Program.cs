﻿namespace NHibernateIntroduction.ConsoleApp
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var work = new WorkWithDb();

            work.Work();

            var workXml = new WorkWithDbXmlMapping();

            workXml.Work();
        }
    }
}