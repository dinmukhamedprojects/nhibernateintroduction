﻿using HumanResources.Database;
using HumanResources.Database.Services;

namespace NHibernateIntroduction.ConsoleApp
{
    public class WorkWithDb
    {
        private DepartmentService _departmentService;

        public void Work()
        {
            var sessionFactory = new HumanResourcesSessionFactoryBuilder(ConfigurationBuilder.Build()).Build().Instance;

            var unitOfWork = new HumanResourcesUnitOfWork(sessionFactory);

            _departmentService = new DepartmentService(unitOfWork);

            var departmentIT = _departmentService.Get("Information Technologies Department");
        }
    }
}