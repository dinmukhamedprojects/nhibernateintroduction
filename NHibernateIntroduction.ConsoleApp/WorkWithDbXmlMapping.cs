﻿using HumanResources.Database.Xml;
using HumanResources.Database.Xml.Services;

namespace NHibernateIntroduction.ConsoleApp
{
    public class WorkWithDbXmlMapping
    {
        public void Work()
        {
            var session = NHibernateHelper.GetCurrentSession();

            var _employeeService = new EmployeeService(session);

            var employee = _employeeService.Get(1);
        }
    }
}