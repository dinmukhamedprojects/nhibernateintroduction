﻿using HumanResources.Database.Xml.Organization;
using NHibernate;
using System.Linq;

namespace HumanResources.Database.Xml.Services
{
    public class EmployeeService
    {
        private readonly ISession _session;

        public EmployeeService(ISession session)
        {
            _session = session;
        }

        public Employee Get(int ID)
        {
            using var tx = _session.BeginTransaction();

            var result = _session.Query<Employee>().SingleOrDefault(x => x.ID == ID);

            tx.Commit();

            return result;
        }
    }
}