﻿using HumanResources.Database.Xml.Organization;
using NHibernate;
using System.Linq;

namespace HumanResources.Database.Xml.Services
{
    public class DepartmentService
    {
        private readonly ISession _session;

        public DepartmentService(ISession session)
        {
            _session = session;
        }

        public Department Get(int ID)
        {
            using var tx = _session.BeginTransaction();

            var result = _session.Query<Department>().SingleOrDefault(x => x.ID == ID);

            tx.Commit();

            return result;
        }
    }
}