﻿using NHibernate;
using NHibernate.Cfg;
using NHibernate.SqlCommand;
using System.Diagnostics;
using System.Reflection;

namespace HumanResources.Database.Xml
{
    public sealed class NHibernateHelper
    {
        private static readonly ISessionFactory _sessionFactory;

        static NHibernateHelper()
        {
            var cfg = new Configuration();
            cfg.Configure();
            cfg.AddAssembly(Assembly.GetExecutingAssembly());

#if (DEBUG)
            var interceptor = new SqlDebugOutputInterceptor();
            cfg.SetInterceptor(interceptor);
#else
    return sessionFactory.OpenSession();
#endif

            _sessionFactory = cfg.BuildSessionFactory();
        }

        public static ISession GetCurrentSession()
        {
            var session = _sessionFactory.OpenSession();

            return session;
        }

        public static void CloseSession()
        {
            _sessionFactory.Close();
        }
    }

    public class SqlDebugOutputInterceptor : EmptyInterceptor
    {
        public override SqlString OnPrepareStatement(SqlString sql)
        {
            Debug.Write("NHibernate: ");
            Debug.WriteLine(sql);

            return base.OnPrepareStatement(sql);
        }
    }
}