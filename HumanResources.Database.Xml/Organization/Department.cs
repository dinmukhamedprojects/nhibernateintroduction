﻿namespace HumanResources.Database.Xml.Organization
{
    public class Department
    {
        public virtual int ID { get; set; }
        public virtual string Name { get; set; }
    }
}