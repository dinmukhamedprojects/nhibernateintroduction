﻿namespace HumanResources.Database.Xml.Organization
{
    public class Employee
    {
        public virtual int ID { get; set; }
        public virtual string FirstName { get; set; }
        public virtual string LastName { get; set; }
    }
}